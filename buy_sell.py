import os
from time import perf_counter as pc

import matplotlib.pyplot as plt
import numpy as np

from mc_gbm import mc_GBM

# =============================================================================

def discount(price, r, dt):
    '''discounts the matrix of prices with countinously compounded int rate
    @r = zero rate(s), scalar or len(r)==len(price)'''
    return (price.T*np.exp(-np.arange(len(price))*dt*r)).T if r else price

def strat(price, buy=True):
    '''naive buy-sell(True)/short-selling(False) strategy'''
    ac = np.minimum.accumulate(price) if buy else np.maximum.accumulate(price)
    earns = price-ac if buy else ac-price
    idx = (earns==np.max(earns)).nonzero()[0][0]
    x = [(ac==ac[idx]).nonzero()[0][0], idx]
    y = [ac[idx], price[idx]]
    return x, y

def opt_strat(price, r, dt):
    '''best ex-post strategy, includes discount rate. 
    returns coords in original price'''
    P = discount(price, r, dt) if r else price
    xb, yb = strat(P)
    xs, ys = strat(P, buy=False)
    if (yb[1]-yb[0]) > (ys[0]-ys[1]):
        return xb, price[xb] # 4 numbers : [[x1,x2], [y1,y2]]
    return xs, price[xs]

# =============================================================================

def __rec_strat(price, r, dt, s, e, fin):
    if e > s:
        P = price[s:e] # slice price
        x, y = opt_strat(P, 0, dt)
        # coords of the strat in original arr
        rx1 = (price==y[0]).nonzero()[0][0] 
        rx2 = (price==y[1]).nonzero()[0][0]
        strat = [rx1, rx2]
        if strat not in fin:
            fin.append(strat)
            fin.sort()
        if fin[0][0]!=0:
            __rec_strat(price, 0, dt, 0, rx1+1, fin)
        if fin[-1][-1]!=(len(price)-1):
            __rec_strat(price, 0, dt, rx2, len(price), fin)

def full_strat(price, r, dt):
    '''returns best buy-sell strat for the whole provided price
    (NO additional money in the strat, only the starting cash)'''
    fin = []
    P = discount(price, r, dt) # like this all strats are relative to t=0
    __rec_strat(P, 0, dt, 0, len(price), fin)  #r=0 (discounted above)
    return fin

# =============================================================================

def iter_strat(price, r, dt):
    P = discount(price, r, dt)
    (x1, x2), _ = opt_strat(P, 0, dt)
    fin = {x1, x2}
    # before opt strat
    while x1:
        tp = P[:x1+1]
        (x1, x2), _ = opt_strat(tp, 0, dt)
        fin.add(x1)
    # after opt strat
    px = x2
    last = len(price)-1
    while (last-px):
        tp = P[px:]
        (x1, x2), _ = opt_strat(tp, 0, dt)
        px += x2 # keep coords on original price
        fin.add(px)
    # sort and organize output
    res = sorted(fin)
    return [[res[i], res[i+1]] for i in range(len(res)-1)]

# ============================================================================

def slice_strat(price, r, dt):
    '''returns best buy-sell strat for the whole provided price
    (WITH additional money in the strat, only the starting cash)'''
    fin = []
    final = [0, len(price)-1]
    first = True
    P = discount(price, r, dt) # discount price here
    while 1: # recursively slice the price arr and get otpimal strat
        if first:
            new_price =  P
            first = False
        else:
            new_price = np.concatenate((P[:s], P[e+1:]))
        x, y = opt_strat(new_price, 0, dt)
        s = (P==y[0]).nonzero()[0][0]
        e = (P==y[1]).nonzero()[0][0]
        S = [s, e]
        if fin==[]:
            fin.append(S)
        else:
            if (fin[-1][0]>s) or (fin[-1][1]<e):
                if S not in fin:
                    fin.append(S)
                else:
                    s = np.min(fin)
                    e = np.max(fin)
        if [s,e]==final:
            break
    fin.sort() # fill in the gaps
    mx = fin[-1][-1]
    for i in range(len(fin)-1):
        if fin[i]!=final:
            a = fin[i][1]
            b = fin[i+1][0]
            if (a!=b) and (a<b):
                fin.append([a,b])
    fin.append([mx, final[-1]])
    fin.append(final)
    return fin

# ============================================================================

def plot_full_strat(price, strat):
    '''nice plot. strat=x-coords of the strat'''
    plt.figure()
    plt.plot(price, linewidth=.5, color="k")
    for i in strat:
        c = "r" if price[i][0]>price[i][1] else "b"
        plt.plot(i, price[i], color=c)
    #plt.show()

# ============================================================================

def ex(price):
    # generic price
    # running min and max
    mins = np.minimum.accumulate(price)
    maxs = np.maximum.accumulate(price) # not needed
    # naive buy-sell strategies
    x,y = strat(price)
    # naive short sell
    xx, yy = strat(price, False)
    plt.figure()
    plt.plot(price, label="price", linewidth=.8)
    plt.plot(mins, label="running minimum", linewidth=.6)
    plt.plot(maxs, label="running maximum", linewidth=.6)
    plt.scatter(x, y, color="k", s=25, label="buy-sell")
    plt.plot(x, y, color="k", linewidth=.4)
    plt.scatter(xx, yy, color="r", s=25, label="short-sell")
    plt.plot(xx, yy, color="r", linewidth=.4)
    plt.legend()
    plt.title("naive optimal strategy")
    #plt.show()

def ex3(price):
    times = len(price)-1
    dt = 1/times
    r = 0.3 # no int rate
    x,y = opt_strat(price, r, dt)
    plt.figure()
    plt.plot(price, color="k", label="original", linewidth=.7)
    plt.plot(x, y, color="k")
    r = .6 # int rate
    discounted_price = discount(price, r, dt)
    dx,dy = opt_strat(price, r, dt)
    plt.plot(discounted_price, color="r", label="discounted", linewidth=.7)
    plt.plot(dx, dy, color="r")
    plt.plot(dx, discounted_price[dx], color="r")
    plt.title("With interest rate = %.4f"%r)
    plt.legend()
    #plt.show()

def ex4(price):
    r = 0.0
    times = len(price)-1
    dt = 1/times
    strat = full_strat(price, r, dt)
##    strat2 = iter_strat(price, r, dt)
##    for i,j in zip(strat, strat2):
##        if i!=j:
##            print("ex4 - different")
    plot_full_strat(price, strat)
    plt.title("Single investment - full time optimal")

def ex5(price):
    r = 0.01
    times = len(price)-1
    dt = 1/times
    x = slice_strat(price, r, dt)
    plot_full_strat(price, x)
    plt.title("Multiple investment - full time optimal")

# ============================================================================

def main():
    rand = np.random.RandomState(seed=3698)
    T = 365
    x = 1#int(input("How many years? "))
    T *= x
    price = mc_GBM(rand, reps=1, times=T, mu=.03, sigma=.4, dt=1/365, s0=78)
    ex(price) # basics
    ex3(price) # optimal with r>0
    ex4(price) # multi-optimal
    ex5(price) # slice
    plt.show()

def speed(reps):
    rand = np.random.RandomState(seed=3698)
    T = 365
    x = 50   #int(input("How many years? "))
    T *= x
    dt = 1/T
    tr = 0
    ti = 0
    print("%10d"%reps)
    for i in range(reps):
        price = mc_GBM(rand, reps=1, times=T, mu=.03, sigma=.4, dt=dt, s0=78)
        sr = pc()
        # x = full_strat(price, .1, dt)
        tr += pc()-sr
        si = pc()
        # y = iter_strat(price, .1, dt)
        ti += pc()-si
        if x!=y:
            print(x)
            print(y)
            plot_full_strat(price, x)
            plot_full_strat(price, y)
            plt.show()
            return
        print("%10d"%i, end="\r", flush=1)
    print("recursive: %3.10f"%tr)
    print("iterative: %3.10f"%ti)
    return

if __name__=="__main__": 
    # _ = os.system("cls")
    main()
    # speed(50)












